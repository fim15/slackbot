import csv
from datetime import datetime
import os

BIRTHDAY_CONFIG = os.path.join(os.path.dirname(__file__),
                               "../config/birthdays.csv")


def birthday_to_datetime(birthday):
    return datetime.strptime(birthday, "%d.%m.%Y")


def set_birthdays(birthdays):
    """
    birthdays is a list of [userid, date] lists
    """

    old_birthdays = list_birthdays()
    for userid, date in birthdays:
        if date is None:
            del old_birthdays[userid]
        else:
            old_birthdays[userid] = date

    with open(BIRTHDAY_CONFIG, 'w') as birthday_config_file:
        writer = csv.writer(birthday_config_file)
        for userid, date in old_birthdays.items():
            writer.writerow([userid, date])


def remove_birthdays(userids):
    set_birthdays([(u, None) for u in userids])


def list_birthdays(users=None, today=False):
    """
    if users is None, all users will be returned
    if today is True, all users whose birthday is today will be returned
    """

    try:
        with open(BIRTHDAY_CONFIG, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='|')
            birthdays = {}
            for row in reader:
                if len(row) < 2:
                    print("[ERROR] invalid row in {}: '{}'".format(
                        BIRTHDAY_CONFIG, row))
                    continue
                userid = row[0]
                birthday = row[1]
                birthdays[userid] = birthday
        if today:
            current_date = datetime.now().date()
            birthdays = {u: b for u, b in birthdays.items()
                         if birthday_to_datetime(b).month == current_date.month
                         and birthday_to_datetime(b).day == current_date.day}
        if users is None:
            return birthdays
        else:
            return {u: b for u, b in birthdays.items() if u in users}
    except FileNotFoundError:
        return {}


def print_birthdays(birthdays):
    def mention(user):
        return "<@{}>".format(user)

    all_birthdays = [(mention(u), birthday_to_datetime(b).date())
                     for u, b in birthdays.items()]
    sorted_birthdays = sorted(
        all_birthdays,
        key=lambda tup: datetime.strftime(tup[1], "%m.%d.%y"))

    today = datetime.now().date()

    try:
        next_birthday = next((u, b) for (u, b) in sorted_birthdays
                             if b.month == today.month and b.day >= today.day
                             or b.month > today.month)
        i = sorted_birthdays.index(next_birthday)
        sorted_birthdays = sorted_birthdays[i:] + sorted_birthdays[:i]
    except StopIteration:
        pass

    text = "\n".join(("{} > {}".format(datetime.strftime(b, "%d.%m.%y"), u)
                      for u, b in sorted_birthdays))
    text = "*FIMily Geburtstage*:\n\n" + text
    return text


if __name__ == '__main__':
    # TODO convert this to a test and use python tmp files
    BIRTHDAY_CONFIG = "/tmp/birthdays.csv"
    print("adding birthdays")
    set_birthdays([("Bob", "24.03.2018"), ("Steven", "13.09.1995")])
    print("Birthdays: {}".format(list_birthdays()))
    print("Bob's birthday: {}".format(list_birthdays(["Bob"])))
    print("removing birthdays")
    remove_birthdays(["Bob"])
    print("Birthdays: {}".format(list_birthdays()))
