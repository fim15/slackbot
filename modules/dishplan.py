import datetime
import requests
from bs4 import BeautifulSoup


def dishplan():
    heading = '*Mensaplan für {}:*\n'\
        .format(datetime.datetime.now().strftime('%d.%m.%Y'))
    dishes = ''
    url_template = 'https://augsburg.my-mensa.de/essen.php?v=5129736'\
                   + '&hyp=1&lang=de&mensa=aug_universitaetsstr_uni'
    req = requests.get(url_template)
    soup = BeautifulSoup(req.content.decode('utf-8'), 'html.parser')
    day_number = datetime.datetime.now().timetuple().tm_yday - 1
    year = datetime.datetime.now().year
    date_id = 'aug_universitaetsstr_uni_tag_' + str(year) + str(day_number)
    current = soup.find(id=date_id)
    li_list = [] if current is None else current.find_all('li')
    for dish in [li_list[i:i+2] for i in range(0, len(li_list), 2)]:
        category = dish[0].find('div').contents[0]
        food = dish[1].find('h3').contents[0].replace('\xad', '').strip()
        additional = ''
        if len(dish[1].find('p', class_='ct text2share')) > 0:
            additional = ' ' + dish[1].find('p', class_='ct text2share')\
                .contents[0].replace('\xad', '').strip()
        price = dish[1].find(class_='ct next text2share').contents[-1]\
                .split('/')[0].strip()
        dishes += '\n*{}*: {}{} ({})'.format(category, food, additional, price)
    return heading + (dishes if dishes != '' else 'Nichts gefunden!')


if __name__ == '__main__':
    print(dishplan())
