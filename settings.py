import os


CHANNELS = {
    'bot-test': 'CD8B60EM7',
    'allgemein': 'CC0ELHM0U',
    'zufällig': 'CC0MR7USW'
}


def get_setting(key):
    try:
        slackbot_settings = __import__('slackbot_settings')
    except ImportError:
        try:
            return os.environ['SLACKBOT_' + key]
        except KeyError:
            return ''
    return getattr(slackbot_settings, key)
