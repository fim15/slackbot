import re
import os

from settings import get_setting
os.environ['SLACKBOT_API_TOKEN'] = get_setting('API_TOKEN')

from slackbot.bot import Bot, listen_to, respond_to  # pylint: disable=wrong-import-position

from modules import dishplan, birthday  # pylint: disable=wrong-import-position

@respond_to('essen', re.IGNORECASE)
@listen_to('speiseplan', re.IGNORECASE)
def speiseplan(message):
    message.reply(dishplan.dishplan())

@listen_to('geburtstage', re.IGNORECASE)
@listen_to('birthdays', re.IGNORECASE)
def birthdays(message):
    message.reply(birthday.print_birthdays(birthday.list_birthdays()))

if __name__ == '__main__':
    BOT = Bot()
    BOT.run()
