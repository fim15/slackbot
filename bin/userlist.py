import os
import sys

from slackclient import SlackClient

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from settings import get_setting # pylint: disable=wrong-import-position


SC = SlackClient(get_setting('API_TOKEN'))

req = SC.api_call("users.list")
if not req['ok']:
    print("[ERROR]: {}".format(req['error']))
    exit()

users = [user for user in req['members'] if not user['is_bot']
         and not user['id'] == "USLACKBOT" and not user['deleted']]

for ind, user in enumerate(users, start=1):
    profile = user['profile']
    print("user #{}: {}, ({}), {}, {}".format(ind, profile['real_name'],
                                              user['name'], profile['email'],
                                              user['id']))
