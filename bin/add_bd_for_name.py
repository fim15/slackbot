import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from datetime import datetime
from name_to_id import find_user
from modules.birthday import set_birthdays

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Usage: add_bd_for_name.py <name> <birthday>")
        exit(1)

    name = sys.argv[1]
    bd = sys.argv[2]
    try:
        bd_parsed = datetime.strptime(bd, "%d.%m.%Y")
    except Exception:
        print("Invalid dateformat, expected: %d.%m.%Y")
        exit(1)

    result = find_user(name)
    if not result:
        print("{} not found!".format(name))
    elif len(result) > 1:
        print("multiple results - please be more spcific")
        print("\n".join(
            ["{} ({})".format(user['id'], user['profile']['real_name'])
             for user in result]))
    else:
        user = result[0]['id']
        set_birthdays([(user, bd)])
