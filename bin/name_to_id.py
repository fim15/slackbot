import os
import sys

from slackclient import SlackClient

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from settings import get_setting # pylint: disable=wrong-import-position

def get_all_users():
    SC = SlackClient(get_setting('API_TOKEN'))

    req = SC.api_call("users.list")
    if not req['ok']:
        print("[ERROR]: {}".format(req['error']))
        exit()

    users = [user for user in req['members'] if not user['is_bot']
             and not user['id'] == "USLACKBOT" and not user['deleted']]
    return users

def find_user(name):
    result = []
    for user in get_all_users():
        profile = user['profile']
        if name.lower() in profile['real_name'].lower() \
                or name.lower().lower() in profile['display_name'].lower() \
                or name.lower() in profile['email'].lower():
            result.append(user)
    return result

if __name__ == '__main__':
    for name in sys.argv[1:]:
        result = find_user(name)
        if not result:
            print("{} not found!".format(name))
        else:
            print("{} -> {}".format(name, ", ".join(
                ["{} ({})".format(user['id'], user['profile']['real_name'])
                 for user in result])))
