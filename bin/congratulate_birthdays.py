import os
import requests
import sys

from slackclient import SlackClient

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from modules.birthday import list_birthdays, print_birthdays # pylint: disable=wrong-import-position
from settings import CHANNELS, get_setting # pylint: disable=wrong-import-position

SC = SlackClient(get_setting('API_TOKEN'))

birthdays_today = list_birthdays(today=True)

for u, _ in birthdays_today.items():
    giphy_url = 'https://api.giphy.com/v1/gifs/random?tag={}&api_key={}'
    giphy_req = requests.get(giphy_url.format('happy+birthday',
                                              get_setting('GIPHY_API_TOKEN')))
    if not giphy_req.status_code == requests.codes.ok:
        print('[ERROR]: Could not retrieve a Happy Birthday GIF!')
        gif_url = ''
    else:
        giphy_json = giphy_req.json()
        gif_url = giphy_json['data']['images']['downsized']['url']

    text = '*Happy Birthday*\n<@{}> hat heute Geburtstag. Alles Gute!\n{}'
    SC.api_call('chat.postMessage', channel=CHANNELS['zufällig'],
                text=text.format(u, gif_url))
