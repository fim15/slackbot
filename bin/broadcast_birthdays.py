import os
import sys

from slackclient import SlackClient

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from modules.birthday import list_birthdays, print_birthdays # pylint: disable=wrong-import-position
from settings import CHANNELS, get_setting # pylint: disable=wrong-import-position

SC = SlackClient(get_setting('API_TOKEN'))

text = print_birthdays(list_birthdays())

SC.api_call('chat.postMessage', channel=CHANNELS['bot-test'], text=text)
