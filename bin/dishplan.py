import os
import sys

from slackclient import SlackClient

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from modules.dishplan import dishplan # pylint: disable=wrong-import-position
from settings import CHANNELS, get_setting # pylint: disable=wrong-import-position


SC = SlackClient(get_setting('API_TOKEN'))


def speiseplan():
    SC.api_call('chat.postMessage',
                channel=CHANNELS['zufällig'],
                text=dishplan())


if __name__ == '__main__':
    speiseplan()
